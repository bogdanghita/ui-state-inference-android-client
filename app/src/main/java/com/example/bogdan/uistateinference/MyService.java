package com.example.bogdan.uistateinference;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

class SmDataInput {

    private String device_id;
    private String device_name;
    private List<List<Long>> data;
    private AppInfo appInfo;

    SmDataInput(String device_id, String device_name, List<List<Long>> data, AppInfo appInfo) {
        this.device_id = device_id;
        this.device_name = device_name;
        this.data = data;
        this.appInfo = appInfo;
    }
}

class AppInfo {

    private String name;
    private String packageName;
    private int pid;
    private boolean running;
    private boolean foreground;

    AppInfo(String name, String packageName) {
        this.name = name;
        this.packageName = packageName;

        this.pid = -1;
        this.running = false;
        this.foreground = false;
    }

    void setRunningTrue(int pid) {
        this.pid = pid;
        this.running = true;
    }

    void setRunningFalse() {
        this.running = false;
    }

    public boolean isRunning() {
        return this.running;
    }

    void setForeground(boolean foreground) {
        this.foreground = foreground;
    }

    int getPid() {
        return this.pid;
    }

    String getPackageName() {
        return packageName;
    }
}

public class MyService extends Service {

    /* NKD */

    static {
        System.loadLibrary("native-lib");
    }

    public native String stringFromJNI();

    public native int sharedMemoryUsage(int pid);


    /* TAGS */

    public final static String TAG_SERVICE = "SERVICE-";
    public final static String TAG_SOCKET = "SOCKET-";
    public final static String TAG_MONITOR = "MONITOR-";
    public final static String TAG_SM = "TAG_SM-";
    public final static String TOP_APP = "TOP_APP-";
    public final static String TAG_BCR = "TAG_BCR-";
    public final static String TAG_UI_EVENT = "TAG_UI_EVENT-";


    /* MONITORING */

    public static final String BCR_PACKAGE_NAME = "at.spardat.bcrmobile";
    public static final String SKYPE_PACKAGE_NAME = "com.skype.raider";

    public final static HashMap<String, AppInfo> targetApps = new HashMap<String, AppInfo>() {{
        put(BCR_PACKAGE_NAME, new AppInfo("BCR Touch 24 Banking", "at.spardat.bcrmobile"));
//        put(SKYPE_PACKAGE_NAME, new AppInfo("Skype", "com.skype.raider"));
    }};

    public static final int BCR_WINDOW_LENGTH_MS = 200;
    public static final int BCR_CONSTANT_EPS = 50;
    public static final double BCR_SPIKE_THRESHOLD_PERCENT = 0.5;
    public static final int BCR_SPIKE_CONSTANT_DIFF = 700;

    private UIInference mUIInference;
    private BCRUIMonitor mBCRUIMonitor;


    /* ACTIVITY & GUI */

    private String DEVICE_ID;
    private final String DEVICE_NAME = android.os.Build.MODEL;

    public static final String ACTION_CLOSE = "action:close";
    public static final int NOTIFICATION_ID = 1100;

    private ActivityManager mActivityManager;

    private NotificationManager mNotificationManager = null;
    private final NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this);

    private int mStartMode;       // indicates how to behave if the service is killed
    private IBinder mBinder;      // interface for clients that bind
    private boolean mAllowRebind; // indicates whether onRebind should be used

    public static boolean RUNNING = false;


    /* SOCKET CONNECTION */

    private final static int serverPort = 9090;
    private final static String serverURL = "http://192.168.1.148:" + serverPort;

    public static final int INPUT_REPORTING_INTERVAL_MS = 20;
    public static final int RUNNING_APPS_MONITORING_INTERVAL_MS = 100;

    private final ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> mInputReportingFuture = null;
    private ScheduledFuture<?> mRunningAppsMonitoringFuture = null;

    private final Object mInputReportingSync = new Object();
    private final Object mRunningAppsMonitoringSync = new Object();

    private Socket mSocket = null;

    {
        try {
            mSocket = IO.socket(serverURL);
        } catch (URISyntaxException e) {
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "Unable to initialize socket. Error=" + e);
        }
    }

    public MyService() {
        mUIInference = new UIInference();
        mBCRUIMonitor = new BCRUIMonitor(mBCRUIEventListener);
        mUIInference.setParams(BCR_PACKAGE_NAME, new UIInference.UIInferenceParams(BCR_WINDOW_LENGTH_MS, BCR_CONSTANT_EPS, BCR_SPIKE_THRESHOLD_PERCENT, BCR_SPIKE_CONSTANT_DIFF));
    }



    /* APP STATES */

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onCreate()");
        RUNNING = true;
        mActivityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        DEVICE_ID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        setupNotifications();
        showNotification();

        subscribeToUIEvents();
        startRunningAppsMonitoring();
        connect();

        test();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onStartCommand()");
        return mStartMode;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onBind()");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onUnbind()");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onRebind()");
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        Log.d(MainActivity.TAG_APP + TAG_SERVICE, "onDestroy()");
        super.onDestroy();
        stopInputReporting();

        disconnect();
        stopRunningAppsMonitoring();
        unsubscribeFromUIEvents();

        cancelNotification();
        RUNNING = false;
    }



    /* NOTIFICATION */

    private void setupNotifications() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        }
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
//                new Intent(this, MainActivity.class)
//                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP),
//                0);
        PendingIntent pendingCloseIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .setAction(ACTION_CLOSE),
                0);
        mNotificationBuilder
                .setSmallIcon(R.mipmap.ic_launcher)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentTitle(getText(R.string.app_name))
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingCloseIntent)
//                .setContentIntent(pendingIntent)
//                .addAction(android.R.drawable.ic_menu_close_clear_cancel, "Exit", pendingCloseIntent)
                .setOngoing(true);
    }

    private void showNotification() {
        String content = "Running in background. Click to close service.";
        mNotificationBuilder
                .setTicker(content)
                .setContentText(content);
        if (mNotificationManager != null) {
            mNotificationManager.notify(NOTIFICATION_ID, mNotificationBuilder.build());
        }
    }

    private void cancelNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(NOTIFICATION_ID);
        }
    }



    /* SOCKET CONNECTION */

    private void connect() {
        if (mSocket == null || mSocket.connected()) {
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "mSocket == null || mSocket.connected()");
            return;
        }
        mSocket.on(Socket.EVENT_CONNECT, mConnectHandler);
        mSocket.on(Socket.EVENT_DISCONNECT, mDisconnectHandler);
        mSocket.on("ev:message", mMessageHandler);
        mSocket.connect();
        Log.d(MainActivity.TAG_APP + TAG_SOCKET, "mSocket.connect()");
    }

    private void disconnect() {
        if (mSocket == null || !mSocket.connected()) {
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "mSocket == null || !mSocket.connected()");
            return;
        }
        mSocket.disconnect();
        mSocket.off("ev:message", mMessageHandler);
        mSocket.off(Socket.EVENT_CONNECT, mConnectHandler);
        mSocket.off(Socket.EVENT_DISCONNECT, mDisconnectHandler);
        Log.d(MainActivity.TAG_APP + TAG_SOCKET, "mSocket.disconnect()");
    }

    private Emitter.Listener mConnectHandler = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "[connect]");
//            mSocket.emit("ev:message", new ArrayList<>(Arrays.asList(1, 2, 3)));
            startInputReporting();
        }
    };

    private Emitter.Listener mDisconnectHandler = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "[disconnect]");
            stopInputReporting();
        }
    };

    private Emitter.Listener mMessageHandler = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            String content = "";
            if (args.length > 0) {
                JSONObject data = (JSONObject) args[0];
                content = data.toString();
//                content = args[0].toString();
            }
            Log.d(MainActivity.TAG_APP + TAG_SOCKET, "[ev:message] data=" + content);
        }
    };



    /* RUNNING APPS */

    private void startRunningAppsMonitoring() {
        synchronized (mRunningAppsMonitoringSync) {
            mRunningAppsMonitoringFuture = mScheduler.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
                    HashMap<String, Integer> pidMap = runnningProcesses();
                    for (Map.Entry<String, AppInfo> entry : targetApps.entrySet()) {
                        String packageName = entry.getKey();
                        AppInfo info = entry.getValue();

                        if (pidMap.containsKey(packageName)) {
                            if (!info.isRunning()) {
                                info.setRunningTrue(pidMap.get(packageName));
                                if (packageName.equals(BCR_PACKAGE_NAME)) {
                                    mBCRUIMonitor.feedUIEvent(new UIInference.UIEvent(packageName, UIInference.UIEventType.PROCESS_START, new Date().getTime()));
                                    // NOTE: PROCESS_FOREGROUND will be fed on the first increase received from the UIInference service
                                }
                            }
                        } else {
                            if (info.isRunning()) {
                                info.setRunningFalse();
                                if (packageName.equals(BCR_PACKAGE_NAME)) {
                                    mBCRUIMonitor.feedUIEvent(new UIInference.UIEvent(packageName, UIInference.UIEventType.PROCESS_STOP, new Date().getTime()));
                                }
                            }
                        }
                    }
                }
            }, 0, RUNNING_APPS_MONITORING_INTERVAL_MS, TimeUnit.MILLISECONDS);
        }
    }

    private void stopRunningAppsMonitoring() {
        synchronized (mRunningAppsMonitoringSync) {
            if (mRunningAppsMonitoringFuture == null) {
                return;
            }
            mRunningAppsMonitoringFuture.cancel(true);
            mRunningAppsMonitoringFuture = null;
        }
    }

    private HashMap<String, Integer> runnningProcesses() {
//        String allAppsLog = "";
        String targetAppsLog = "";
        HashMap<String, Integer> targetPids = new HashMap<>();

        ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> pidsTask = activityManager.getRunningAppProcesses();

        for (int i = 0; i < pidsTask.size(); i++) {
            ActivityManager.RunningAppProcessInfo processInfo = pidsTask.get(i);
            String name = processInfo.processName;
            int pid = processInfo.pid;
            if (targetApps.containsKey(name)) {
                targetPids.put(name, pid);
                targetAppsLog += name + ", " + pid + " | ";
            }
//            allAppsLog += name + ", " + pid + " | ";
        }

//        Log.d(MainActivity.TAG_APP + TOP_APP, allAppsLog);
//        Log.d(MainActivity.TAG_APP + TOP_APP, targetAppsLog);
        return targetPids;
    }



    /* INPUT REPORTING */

    private void startInputReporting() {
        synchronized (mInputReportingSync) {
            mInputReportingFuture = mScheduler.scheduleAtFixedRate(new Runnable() {
                @Override
                public void run() {
//                  sendSmData(generateMockSmData(1));
                    for (Map.Entry<String, AppInfo> entry : targetApps.entrySet()) {
                        String name = entry.getKey();
                        AppInfo info = entry.getValue();

                        LinkedList<List<Long>> data = new LinkedList<>();
                        if (info.isRunning()) {
                            List<Long> item = getSmItem(info.getPid());
                            mUIInference.feedValue(info.getPackageName(), item.get(1));
                            data.add(item);
                        } else {
                            mUIInference.feedValue(info.getPackageName(), 0);
                        }
                        sendSmData(data, info);
                    }
                }
            }, 0, INPUT_REPORTING_INTERVAL_MS, TimeUnit.MILLISECONDS);
        }
    }

    private void stopInputReporting() {
        synchronized (mInputReportingSync) {
            if (mInputReportingFuture == null) {
                return;
            }
            mInputReportingFuture.cancel(true);
            mInputReportingFuture = null;
        }
    }

    private void sendSmData(List<List<Long>> data, AppInfo appInfo) {
        SmDataInput content = new SmDataInput(DEVICE_ID, DEVICE_NAME, data, appInfo);
        String jsonContent = new Gson().toJson(content);
        mSocket.emit("input:sm", jsonContent);
    }

    private List<Long> getSmItem(int pid) {
        int smValue = sharedMemoryUsage(pid);
//        Log.d(MainActivity.TAG_APP+TAG_SM, String.valueOf(smValue));
        long timestamp = new Date().getTime();
        List<Long> item = new ArrayList<>();
        item.add(timestamp);
        item.add((long) smValue);
        return item;
    }



    /*UI Inference*/

    private void subscribeToUIEvents() {
        for (Map.Entry<String, AppInfo> entry : targetApps.entrySet()) {
            mUIInference.subscribe(entry.getKey(), mUIEventListener);
        }
    }

    private void unsubscribeFromUIEvents() {
        for (Map.Entry<String, AppInfo> entry : targetApps.entrySet()) {
            mUIInference.unsubscribe(entry.getKey(), mUIEventListener);
        }
    }

    UIInference.UIEventListener mUIEventListener = new UIInference.UIEventListener() {
        @Override
        public void notifyEvent(UIInference.UIEvent event) {
            mBCRUIMonitor.feedUIEvent(event);
        }
    };

    BCRUIMonitor.BCRUIEventListener mBCRUIEventListener = new BCRUIMonitor.BCRUIEventListener() {
        @Override
        void notifyEvent(BCRUIMonitor.BCREvent event) {
            Log.d(MainActivity.TAG_APP + TAG_BCR, event.toString());
            switch (event) {
                case PROCESS_START:
                    break;
                case PROCESS_STOP:
                    break;
                case PROCESS_FOREGROUND:
                    break;
                case PROCESS_BACKGROUND:
                    break;
                case OPEN_LOGIN:
                    openFakeBCRLoginPage();
                    break;
            }
        }
    };

    private void openFakeBCRLoginPage() {
        Intent intent = new Intent(this, FakeBCRLoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }



    /* TEST & DEBUG */

    private void test() {
//        String result = foregroundProcess();
//        Log.d(MainActivity.TAG_APP+TOP_APP, result);

//        runnningProcesses();
    }

    private List<List<Long>> generateMockSmData(int size) {
//        String content = "MonitorTask.run() " + stringFromJNI();
//        String content = "MonitorTask.run() " + sharedMemoryUsage();
//        Log.d(MainActivity.TAG_APP + TAG_MONITOR, content);

        LinkedList<List<Long>> data = new LinkedList<>();
        for (int i = 0; i < size; i++) {
            List<Long> item = new ArrayList<>();
            item.add(new Date().getTime());
            item.add((long) (new Random().nextInt(20)));
            data.add(item);
        }
        return data;
    }

    private String foregroundProcess() {
        String packageName = null;
        int processPID = -1;

        if (Build.VERSION.SDK_INT > 20) {
            ActivityManager.RunningAppProcessInfo processInfo = mActivityManager.getRunningAppProcesses().get(0);
            packageName = processInfo.processName;
            processPID = processInfo.pid;
        } else {
            ActivityManager.RunningTaskInfo taskInfo = mActivityManager.getRunningTasks(1).get(0);
            packageName = taskInfo.topActivity.getPackageName();
            processPID = -100;
        }

        return packageName + ", " + processPID;
    }
}
