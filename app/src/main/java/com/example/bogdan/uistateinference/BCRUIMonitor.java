package com.example.bogdan.uistateinference;

import android.util.Log;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by bogdan on 07.01.2017.
 */

public class BCRUIMonitor {

    public enum BCREvent {PROCESS_START, PROCESS_FOREGROUND, OPEN_LOGIN, PROCESS_BACKGROUND, PROCESS_STOP}

    public abstract static class BCRUIEventListener {
        abstract void notifyEvent(BCREvent event);
    }

    private BCRUIEventListener listener;
    private List<UIInference.UIEvent> eventsHistory;

    public BCRUIMonitor(BCRUIEventListener listener) {
        this.listener = listener;
        this.eventsHistory = new LinkedList<>();
    }

    public void feedUIEvent(UIInference.UIEvent event) {
        eventsHistory.add(event);

        switch (event.type) {
            case PROCESS_START:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.PROCESS_START);
                if (eventsHistory.size() > 1) {
                    eventsHistory.clear();
                    eventsHistory.add(event);
                }
                listener.notifyEvent(BCREvent.PROCESS_START);
                break;

            case PROCESS_STOP:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.PROCESS_STOP);
                eventsHistory.clear();
                listener.notifyEvent(BCREvent.PROCESS_STOP);
                break;

            case HIGH_SPIKE:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.HIGH_SPIKE);
                checkEventPattern();
                break;

            case LOW_SPIKE:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.LOW_SPIKE);
                checkEventPattern();
                break;

            case INCREASE:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.INCREASE);
                checkEventPattern();
                break;

            case DECREASE:
                Log.d(MainActivity.TAG_APP + MyService.TAG_UI_EVENT, "[" + event.appId + "]" + UIInference.UIEventType.DECREASE);
                checkEventPattern();
                break;
        }
    }

    private void checkEventPattern() {
        int length = eventsHistory.size();
        if (length >= 3 &&
                eventsHistory.get(length - 1).type == UIInference.UIEventType.HIGH_SPIKE &&
                eventsHistory.get(length - 2).type == UIInference.UIEventType.HIGH_SPIKE &&
                eventsHistory.get(length - 3).type == UIInference.UIEventType.INCREASE) {
            Log.d(MainActivity.TAG_APP + MyService.TAG_BCR, "BCR login event detection");
            listener.notifyEvent(BCREvent.OPEN_LOGIN);
        } else {
            Log.d(MainActivity.TAG_APP + MyService.TAG_BCR, "No BCR event detection");
        }
    }
}
