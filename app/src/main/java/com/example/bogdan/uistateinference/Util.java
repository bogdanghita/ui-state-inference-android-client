package com.example.bogdan.uistateinference;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by bogdan on 08.01.2017.
 */

public class Util {

    public static class IterativeMean {
        private double avg;
        private int t;

        public IterativeMean(){
            this.avg = 0;
            this.t = 1;
        }

        public double addValue(double value){
            this.avg += (value - this.avg) / t;
            this.t += 1;
            return this.avg;
        }
    }
}
