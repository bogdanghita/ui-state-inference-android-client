package com.example.bogdan.uistateinference;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;


public class MainActivity extends AppCompatActivity {

    public final static String TAG_APP = "@APP-";
    private final static String TAG_MAIN = "MAIN-";
    private final static String TAG_INTENT = "INTENT-";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onCreate]");

        if (!MyService.RUNNING) {
            startBackgroundService();
            finish();
        }
        handleIntent(getIntent());
    }

    protected void onStart() {
        super.onStart();
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onStart]");
    }

    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onResume]");
    }

    protected void onPause() {
        super.onPause();
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onPause]");
    }

    protected void onStop() {
        super.onStop();
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onStop]");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(MainActivity.TAG_APP + TAG_MAIN, "[onDestroy]");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent){
        String action = intent.getAction();
        Log.d(MainActivity.TAG_APP + TAG_INTENT, "[onNewIntent] action=" + action);
        if (action.equals(MyService.ACTION_CLOSE)) {
            if (MyService.RUNNING) {
                stopBackgroundService();
                finish();
            }
        }
    }

    private void startBackgroundService() {
        Intent intent = new Intent(this, MyService.class);
        startService(intent);
    }

    private void stopBackgroundService() {
        Intent intent = new Intent(this, MyService.class);
        stopService(intent);
    }
}
