package com.example.bogdan.uistateinference;

import android.util.Log;

import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by bogdan on 07.01.2017.
 */

public class UIInference {

    public static final String TAG_UI_INF = "TAG_UI_INF-";

    /* public classes */

    public static class UIInferenceParams {
        public int windowLengthMs;
        public int constantEps;
        public double spikeThresholdPercent;
        public int spikeConstantDiff;

        public UIInferenceParams(int windowLengthMs, int constantEps, double spikeThresholdPercent, int spikeConstantDiff) {
            this.windowLengthMs = windowLengthMs;
            this.constantEps = constantEps;
            this.spikeThresholdPercent = spikeThresholdPercent;
            this.spikeConstantDiff = spikeConstantDiff;
        }
    }

    public enum UIEventType {
        PROCESS_START, PROCESS_STOP,
        HIGH_SPIKE, LOW_SPIKE, INCREASE, DECREASE
    }

    public static class UIEvent {

        public String appId;
        public UIEventType type;
        public long timestamp;

        public UIEvent(String appId, UIEventType type, long timestamp) {
            this.appId = appId;
            this.type = type;
            this.timestamp = timestamp;
        }
    }

    public abstract static class UIEventListener {
        public abstract void notifyEvent(UIEvent event);
    }


    /* private classes */

    private enum PeriodType {CONSTANT, DISTURBANCE}

    private static class Item {
        public long timestamp;
        public long value;

        public Item(long timestamp, long value) {
            this.timestamp = timestamp;
            this.value = value;
        }
    }

    private class AppData {

        private class Disturbance {
            public long start, end;
            public long minV, maxV;
            public long levelDiff;

            public Disturbance(Item firstItem) {
                this.start = firstItem.timestamp;
                this.minV = firstItem.value;
                this.maxV = firstItem.value;
            }

            @Override
            public String toString() {
                String content = "start=";
                content += String.valueOf(this.start) + ", end=";
                content += String.valueOf(this.end) + ", minV=";
                content += String.valueOf(this.minV) + ", maxV=";
                content += String.valueOf(this.maxV) + ", levelDiff=";
                content += String.valueOf(this.levelDiff);
                return content;
            }
        }

        private class Constant {
            public long start, end;
            public double avg;
            Util.IterativeMean itMean;

            public Constant() {
                this.start = new Date().getTime();
                this.end = new Date().getTime();
                this.avg = 0;
                itMean = new Util.IterativeMean();
            }

            @Override
            public String toString() {
                String content = "start=";
                content += String.valueOf(this.start) + ", end=";
                content += String.valueOf(this.end) + ", avg=";
                content += String.valueOf(this.avg);
                return content;
            }
        }

        String appId;
        List<UIEventListener> listeners;
        UIInferenceParams params;
        Disturbance disturbance = null;
        Constant constant = null;
        PeriodType currentPeriod = null;

        Item prevItem;
        Deque<Item> constantValues;
        double constantValuesAvg = 0;

        AppData(String appId) {
            this.appId = appId;
            this.listeners = new LinkedList<>();
            this.constantValues = new LinkedList<>();
            this.params = new UIInferenceParams(200, 50, 0.5, 300);

            this.constant = new Constant();
        }

        void feedItem(Item item) {
            if (constantValues.isEmpty()) {

                currentPeriod = PeriodType.DISTURBANCE;
                disturbance = new Disturbance(item);
                constantValues.offerFirst(item);

            } else if (currentPeriod == PeriodType.DISTURBANCE) {

                // check against values in current window and see if all of them are in the same range
                updateConstantValues(item);
                long constantLength = constantValues.peekLast().timestamp - constantValues.peekFirst().timestamp;
                if (constantLength >= params.windowLengthMs) {
                    // Create new constant
                    Constant newConstant = new Constant();
                    newConstant.start = constantValues.peekFirst().timestamp;
                    newConstant.avg = constantValuesAvg;

                    // Set final disturbance params
                    disturbance.end = item.timestamp;
                    disturbance.levelDiff = (long) (constant.avg - newConstant.avg);

                    currentPeriod = PeriodType.CONSTANT;

                    // Report disturbance
                    reportDisturbance(disturbance, item.timestamp, constant, newConstant);

                    // Update constant
                    constant = newConstant;
                } else {
                    // Update disturbance params
                    if (item.value > disturbance.maxV) {
                        disturbance.maxV = item.value;
                    }
                    if (item.value < disturbance.minV) {
                        disturbance.minV = item.value;
                    }
                }

            } else if (currentPeriod == PeriodType.CONSTANT) {

                // check if value is in the interval avg +- eps
                if (Math.abs(constant.avg - item.value) <= params.constantEps) {
                    // if ok, update avg
                    constant.avg = constant.itMean.addValue((double) item.value);
                } else {
                    // set final constant params
                    constant.end = prevItem.timestamp;
                    // init disturbance
                    currentPeriod = PeriodType.DISTURBANCE;
                    disturbance = new Disturbance(item);
                    constantValues.clear();
                    constantValues.offerFirst(item);
                }
            }

            prevItem = item;
        }

        void updateConstantValues(Item newItem) {
            if (constantValues.isEmpty()) {
                constantValues.offerLast(newItem);
                constantValuesAvg = newItem.value;
                return;
            }

            long sum = 0;
            int cnt = 0;

//            Item last = constantValues.peekLast();
//            while (!constantValues.isEmpty()) {
//                last = constantValues.peekLast();
//                if (Math.abs(newItem.value - last.value) > params.constantEps) {
//                    // stop and delete this one and all others
//                    break;
//                }
//                sum += last.value;
//                cnt += 1;
//            }
//
//            // delete all values until you reach 'last'
//            while (!constantValues.isEmpty() && (constantValues.peekFirst() != last)) {
//                constantValues.pollFirst();
//            }
//            constantValues.pollFirst();

            Iterator<Item> it = constantValues.descendingIterator();
            while (it.hasNext()) {
                Item item = it.next();
                if (Math.abs(newItem.value - item.value) > params.constantEps) {
                    // stop and delete this one and all others
                    break;
                }
                sum += item.value;
                cnt += 1;
            }
            if (it.hasNext()) {
                it.remove();
            }
            while (it.hasNext()) {
                it.next();
                it.remove();
            }

            // add new value
            constantValues.offerLast(newItem);
            sum += newItem.value;
            cnt += 1;

            // update average
            constantValuesAvg = sum / cnt;
        }

        void reportDisturbance(Disturbance disturbance, long timestamp, Constant oldConstant, Constant newConstant) {
            Log.d(MainActivity.TAG_APP + TAG_UI_INF, "[" + disturbance.toString() + "], [" + oldConstant.toString() + "], [" + newConstant.toString() + "]");

            boolean increase = false;
            boolean decrease = false;
            boolean pos_spike = false;
            boolean neg_spike = false;
            boolean isolated_spike = false;

            long max_c, min_c;
            if (oldConstant.avg > newConstant.avg) {
                max_c = (long) oldConstant.avg;
                min_c = (long) newConstant.avg;
            } else {
                max_c = (long) newConstant.avg;
                min_c = (long) oldConstant.avg;
            }

            // increase/decrease
            if (Math.abs(disturbance.levelDiff) > params.constantEps) {
                if (disturbance.levelDiff > 0) {
                    decrease = true;
                } else {
                    increase = true;
                }
            }

            // high/low spike
            double spike_th = params.spikeThresholdPercent * Math.abs(disturbance.levelDiff);
            if (disturbance.maxV > max_c + spike_th) {
                pos_spike = true;
            }
            if (disturbance.minV < min_c - spike_th) {
                neg_spike = true;
            }

            // isolated spike
            if (Math.abs(disturbance.levelDiff) < params.spikeConstantDiff && (pos_spike ^ neg_spike)) {
                isolated_spike = true;
            }

            Log.d(MainActivity.TAG_APP + TAG_UI_INF, "increase: " + increase + ", decrease: " + decrease + ", pos_spike: " + pos_spike + ", neg_spike: " + neg_spike + ", isolated_spike: " + isolated_spike);

            // detect event type
            UIEventType eventType = null;
            if (isolated_spike) {
                eventType = pos_spike ? UIEventType.HIGH_SPIKE : UIEventType.LOW_SPIKE;
            } else if (increase) {
                eventType = UIEventType.INCREASE;
            } else if (decrease) {
                eventType = UIEventType.DECREASE;
            }

            if (eventType == null) {
                Log.d(MainActivity.TAG_APP + TAG_UI_INF, "[ERROR] could not decide event type");
                return;
            }

            Log.d(MainActivity.TAG_APP + TAG_UI_INF, eventType.toString());

            UIEvent event = new UIEvent(appId, eventType, timestamp);
            UIInference.this.notifyListeners(this.appId, event);
        }
    }


    /* UIInference class implementation */

    private HashMap<String, AppData> apps;

    public UIInference() {
        this.apps = new HashMap<>();
    }

    public void setParams(String appId, UIInferenceParams params) {
        if (!apps.containsKey(appId)) {
            apps.put(appId, new AppData(appId));
        }
        apps.get(appId).params = params;
    }

    public void feedValue(String appId, long value) {
        if (!apps.containsKey(appId)) {
            apps.put(appId, new AppData(appId));
        }
        AppData appData = apps.get(appId);
        long timestamp = new Date().getTime();
        Item newItem = new Item(timestamp, value);

        appData.feedItem(newItem);
    }

    public void subscribe(String appId, UIEventListener listener) {
        if (!apps.containsKey(appId)) {
            apps.put(appId, new AppData(appId));
        }
        apps.get(appId).listeners.add(listener);
    }

    public void unsubscribe(String appId, UIEventListener listener) {
        if (!apps.containsKey(appId)) {
            return;
        }
        apps.get(appId).listeners.remove(listener);
    }

    private void notifyListeners(String appId, UIEvent event) {
        if (!apps.containsKey(appId)) {
            return;
        }
        for (UIEventListener listener : apps.get(appId).listeners) {
            listener.notifyEvent(event);
        }
    }
}
