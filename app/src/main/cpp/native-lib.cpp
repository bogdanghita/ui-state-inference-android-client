#include <jni.h>
#include <string>
#include <stdio.h>
#include <iostream>
#include <errno.h>
#include <sstream>
#include <sys/types.h>
#include <unistd.h>


extern "C"
jstring
Java_com_example_bogdan_uistateinference_MyService_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {

    std::ostringstream hello_oss;

    /* PID + CREATE FILENAME */

    errno = 0;

//    pid_t pid = getpid();
    pid_t pid = 2915;
    hello_oss << "pid=" << pid;

    std::ostringstream filename_oss;
    filename_oss << "/proc/" << pid << "/statm";
//    filename_oss << "/proc/15920/statm";
    std::string filename = filename_oss.str();

    if(errno != 0){
        hello_oss << ", [pid+create_filename]errno=" << errno;
        return env->NewStringUTF(hello_oss.str().c_str());
    }

    /* FILE OPERATION */

    errno = 0;
    FILE *file = fopen(filename.c_str(), "r");
    if (file != NULL) {
        int size, resident, shared, text, lib, data, dt;

        fscanf(file, "%d %d %d %d %d %d %d", &size, &resident, &shared, &text, &lib, &data, &dt);
        fclose(file);

        hello_oss << ", statm=[size=" << size << ", resident=" << resident << ", shared=" << shared << \
            ",text=" << text << ", lib=" << lib << ",data=" << data << ", dt=" << dt << "]";
    }
    if(errno != 0) {
        hello_oss << ", [file_operation]errno=" << errno;
        return env->NewStringUTF(hello_oss.str().c_str());
    }

    /* END */

    hello_oss << ", [success]errno=" << errno;
    return env->NewStringUTF(hello_oss.str().c_str());
}

extern "C"
jint
Java_com_example_bogdan_uistateinference_MyService_sharedMemoryUsage(
        JNIEnv *env,
        jobject /* this */,
        jint pid) {

    errno = 0;

    std::ostringstream filename_oss;
    filename_oss << "/proc/" << pid << "/statm";
    std::string filename = filename_oss.str();

    /* FILE OPERATION */
    int size, resident, shared=-1, text, lib, data, dt;

    errno = 0;
    FILE *file = fopen(filename.c_str(), "r");
    if (file != NULL) {
        fscanf(file, "%d %d %d %d %d %d %d", &size, &resident, &shared, &text, &lib, &data, &dt);
        fclose(file);
    }
    if(errno != 0) {
        return errno;
    }

    return shared;
}